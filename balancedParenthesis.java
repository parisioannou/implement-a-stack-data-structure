
public class balancedParenthesis {

	//constructor initialises the instance variables, creates a new stack
	static class stack  
    { 
        int s=-1; 
        char stacks[] = new char[100]; 
        
        //push methos to add elements
        void push(char x)  
        { 
            if (s == 99)  
            { 
                System.out.println("Stack is full"); 
            }  
            else 
            { 
                stacks[++s] = x; 
            } 
        } 
        
        //pop method, returns the top element
        char pop()  
        { 
            if (s == -1)  
            { 
                System.out.println("Cannot return an element"); 
                return '\0'; 
            }  
            else 
            { 
                char node = stacks[s]; 
                s--; 
                return node; 
            } 
            
        }
            boolean isEmpty()  
            { 
                return (s == -1) ? true : false; 
            } 
}
	
	
	//Returns true if char1 and char2 
    //are matching left and right Parenthesis
 static boolean isMatchingPair(char char1, char char2) 
 { 
    if (char1 == '(' && char2 == ')') 
      return true; 
    else if (char1 == '{' && char2 == '}') 
      return true; 
    else if (char1 == '[' && char2 == ']') 
      return true; 
    else
      return false; 
 } 
 
        //Return true if expression has balancedParenthesis
     static boolean areParenthesisBalanced(char bal[]) 
     { 
        //Declare an empty character stack
        stack st=new stack(); 
        
        //Traverse the given expression to check matching parenthesis
        for(int i=0;i<bal.length;i++) 
        { 
             
           //If the bal[i] is a starting  
             //parenthesis then push it
           if (bal[i] == '{' || bal[i] == '(' || bal[i] == '[') 
             st.push(bal[i]); 
        
           //If bal[i] is an ending parenthesis  
             //then pop from stack and check if the  
              //popped parenthesis is a matching pair
           if (bal[i] == '}' || bal[i] == ')' || bal[i] == ']') 
           { 
               
               //If we see an ending parenthesis without a pair then return false
              if (st.isEmpty()) 
                { 
                    return false; 
                }  
        
              // Pop the top element from stack
              else if ( !isMatchingPair(st.pop(), bal[i]) ) 
                { 
                    return false; 
                } 
           } 
        }
        
        
     if (st.isEmpty()) 
       return true; 
     else
       { 
           return false; 
       }  
  }  
     
   
 } 
