# Implementation of a stack data structure in Java

A small project in Java created in the 2nd year of my studies.

A function called balancedParenthesis, that reads a string including parentheses, square brackets,
and curly braces from standard input and uses a stack to determine whether they are properly
balanced.
